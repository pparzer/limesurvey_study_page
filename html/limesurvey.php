<?php
// Functions to get list of active Limesurvey forms for code


// execute query and check for errors
function ls_query($conn, $query) {
  // error_log("execute query: ". $query);
  $res = $conn->query($query);
  if ($res == FALSE) {
    throw new \Exception("Error executing query: " . $query);
  }
  return $res;
}


// find all active limesurvey forms for code
function ls_surveys($code)
{
  global $db_host, $db_database, $db_user, $db_password, $ls_owner, $ls_url;
  $forms = [];
  
  $mysqli = new mysqli($db_host, $db_user, $db_password, $db_database);
	if ($mysqli->connect_errno) {
		throw new \Exception("Failed to connect to MySQL: " . $mysqli->connect_error);
  }

  // get list of active surveys for owner
  $res = ls_query($mysqli, "SELECT x.sid, y.surveyls_title" . 
                           " FROM lime_surveys as x" .
                           " JOIN lime_surveys_languagesettings as y" .
                           " ON x.sid=y.surveyls_survey_id" .
                           " WHERE active='Y' AND owner_id=" . $ls_owner .
                           " AND (expires is NULL OR expires>NOW())" .
                           " AND (startdate is NULL OR startdate<NOW());");

  // loop over all active surveys of the owner 
  // and add all active surveys for code to the list of surveys
  while($row = $res->fetch_assoc()) {
    $p = ls_query($mysqli, "SELECT validfrom, validuntil". 
                           " FROM lime_tokens_" . $row["sid"] . 
                           " WHERE token='". $code . "' and completed='N'".
                           " and (validfrom is NULL or validfrom<NOW())" .
                           " and (validuntil is NULL or validuntil>NOW());");

    if ($p->num_rows > 0) {
      // error_log("found survey '" . $row["surveyls_title"]);
      $tok = $p->fetch_assoc();
 		  $forms[] = ["validfrom"  => $tok["validfrom"],
		              "validuntil" => $tok["validuntil"],
		              "url"        => $ls_url."/".$row["sid"]."/token/".$code,
		              "title"      => $row["surveyls_title"]];
    }
  }
  
	return $forms;
}

// helper function for debugging
function dump($data) {
	ob_start();
	var_dump($data);
	$buffer = ob_get_contents();
	ob_end_clean();
	error_log($buffer);
}

?>
