<?php
//ini_set('display_errors', 1);
//error_reporting(E_ALL);
session_start();

$code = "";
if (isset($_GET['code'])) {
	$code = $_GET['code'];
} elseif (isset($_SESSION['code'])) {
	$code = $_SESSION['code'];
}

if ($code != "") {
	header("Location: surveys.php?code=".$code);
	exit();
}

include_once 'config.php';
include_once 'lang_'.$lang.'.php';

?>
<!DOCTYPE html>
<html>
<head>
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="expires" content="0">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="style.css">
</head>

<body>
<div class="content">

<?php
if (!empty($header1)) echo "<h1>".$header1."</h1>";
if (!empty($header2)) echo "<h2>".$header2."</h2>";
if (!empty($header3)) echo "<h3>".$header3."</h3>";

if (file_exists("offline.php")) {
	include "offline.php";
} else {
	echo '<form action="surveys.php" method="post">' .
	  '<p style="text-align:center; padding: 1em 0 1em 0">' .
	  MSG_1 . ':&nbsp;<input type="text" name="code" autofocus /></p>' .
	  '<p style="text-align:center">' .
	  '<input type="Submit" value="' . MSG_2 . '" /></p></form>';
}
?>
</div>
</body>
</html>

