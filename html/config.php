<?php

// page headers
$header1 = 'Example Study';
$header2 = 'University';
$header3 = 'Institution';

// parameters to connect to the database
$db_host     = 'localhost';
$db_database = 'limesurvey';
$db_user     = 'limesurvey';
$db_password = 'limesurvey user password';

// parameters for limesurvey
$ls_url = 'https://www.example.com/limesurvey/index.php';
$ls_owner = 1;

// mail error messages
$mailto = 'administrator@example.com';
$subject = 'Limesurvey Study Page Error';
$header = 'Content-Type: text/plain; charset=utf-8';

// set language for messages
// make sure the corresponding language file exists
// (lang_en.php for 'en', lang_de.php for 'de' etc.)
$lang = 'en';
?>
