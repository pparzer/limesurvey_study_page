<?php
const MSG_1 = 'Code';
const MSG_2 = 'Absenden';
const MSG_3 = 'Zur Zeit ist leider keine Verbindung zu den Online-Formularen möglich.';
const MSG_4 = 'Versuchen Sie es bitte erneut in einigen Minuten.';
const MSG_5 = 'Bitte einen gültigen Code eingeben';
const MSG_6 = 'Zur Zeit gibt es keine Online-Formulare für Code';
const MSG_7 = 'Online-Formulare für Code';
const MSG_8 = 'Einen neuen Code eingeben';
const MSG_9 = 'Bei der Durchführung des Fragebogens ist leider ein Fehler aufgetreten.';
const MSG_10 = 'Zurück zur Startseite';
const MSG_11 = 'Fehler bei der Suche nach den Umfragen';
?>
