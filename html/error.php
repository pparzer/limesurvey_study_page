<?php
//ini_set('display_errors', 1);
//error_reporting(E_ALL);
include_once 'config.php';
include_once 'lang_'.$lang.'.php';
?>

<!DOCTYPE html>
<html>
<head>
<?php 
echo "<title>$header1</title>";
?>
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="expires" content="0">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>

<div class="content">

<?php
if (!empty($header1)) echo "<h1>".$header1."</h1>";
if (!empty($header2)) echo "<h2>".$header2."</h2>";
if (!empty($header3)) echo "<h3>".$header3."</h3>";

echo '<p style="text-align:center; color:red; margin-bottom:1em">' .
  '<strong>' . MSG_9 . '</br></strong></p>' .
  '<p style="text-align:center"><a href="index.php">' . MSG_10 . '</a></p>';
?>

</div>
</body>
</html>
