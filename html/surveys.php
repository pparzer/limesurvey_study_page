<?php
//ini_set('display_errors', 1);
//error_reporting(E_ALL);
session_start();

include_once 'config.php';
include_once 'lang_'.$lang.'.php';
include_once 'limesurvey.php';
?>

<!DOCTYPE html>
<html>
<head>
<?php 
echo "<title>$header1</title>";
?>
<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8">
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="expires" content="0">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>

<div class="content">

<?php
if (!empty($header1)) echo "<h1>".$header1."</h1>";
if (!empty($header2)) echo "<h2>".$header2."</h2>";
if (!empty($header3)) echo "<h3>".$header3."</h3>";

// get token
$code = "";
if (isset($_GET['code'])) {
	$code = $_GET['code'];
} elseif (isset($_SESSION['code'])) {
	$code = $_SESSION['code'];
} elseif (isset($_POST['code'])) {
	$code = $_POST['code'];
}

$code = trim($code);

if (! ctype_alnum($code)) {

	unset($_SESSION['code']);
	echo "<p style=\"text-align:center; color:red; margin-bottom:1em\">";
	echo "<strong>". MSG_5 . "</strong>";
	echo "<p>";

} else {

	$_SESSION['code'] = $code;
	
	// find active surveys
	try {
		$surveys = ls_surveys($code);

		if (count($surveys) == 0) {

			echo "<p style=\"text-align:center; margin-bottom:1em\">";
			echo MSG_6 . " <strong>$code</strong>";
			echo "<p>";

		} else {

			echo "<p>" . MSG_7 . " <strong>$code</strong></p>";
			echo "<ul>";

			sort($surveys);

			foreach ($surveys as $s) {
				$url = $s["url"];
				$title = $s["title"];
				echo "<li><a href=\"$url\"><strong>$title</strong></a></li>";
			}
			echo "</ul>";
		} 
	} catch (Exception $e) {

		echo '<p style="text-align:center; color:red; margin-bottom:1em">' . 
		  '<strong>' . MSG_3 . '<br>' . MSG_4 . '</strong></p>';

  	error_log(MSG_11 . ": " . $e->getMessage());
		if (!empty($mailto)) {
			mail($mailto, $subject, MSG_11 . ": " . $e->getMessage(), $header);
		}
	}

}

echo '<p style="text-align:center">' .
  '<a href="restart.php">' . MSG_8 . '</a></p>';
?>

</div>
</body>
</html>
