<?php
const MSG_1 = 'Code';
const MSG_2 = 'Submit';
const MSG_3 = 'The study survey page is currently offline.';
const MSG_4 = 'Please try again in a few minutes.';
const MSG_5 = 'Please enter a valid code';
const MSG_6 = 'No surveys for code';
const MSG_7 = 'Surveys for code';
const MSG_8 = 'Enter a new code';
const MSG_9 = 'Unfortunately, an error occurred while completing the questionnaire.';
const MSG_10 = 'Back to the home page';
const MSG_11 = 'Error find surveys';
?>
