
# LimeSurvey Study Page

If you are using LimeSurvey questionnaires for your study, then you will likely have multiple questionnaires at different times for each participant. It can be cumbersome and error-prone if you have to send each participant a separate link for each and every questionnaire.

Another approach is to create a web page for the study where participants enter their personal code and then see the list of questionnaires that are currently active for them. By clicking on one of the questionnaires, the participant is forwarded to the corresponding input form. After completing the questionnaire, the participant is returned to the list of remaining questionnaires.

This project provides scripts and instructions for implementing such a solution as a front end for LimeSurvey.

## Prerequsites

- A web server with PHP.

- A LimeSurvey server.

- Direct database access to the LimeSurvey database (the LimeSurvey RemoteControl API is to slow for projects with many questionnaires).

## Installation

- Create a new user in LimeSurvey. Make this user the owner of all questionnaires used by the study. Make sure that this user does not have any other questionnaires. The PHP scripts look for active questionnaires for a participant in this user's questionnaires.

- Create a directory in your www-root on your web server, e.g. `/var/www/html/mystudy`.

- Copy all files from the directory `html` to the new directory of your web server.

- Configure your study page (see below).

- [Optional] Create a virtual host for your study page (see below).


### Configuration

Edit the file `config.php`.

Page headers of your study page. They are displayed just above the code input field.

$header1
: The content of this variable is displayed with a `<h1>` tag as the header of the study page.

$header2, $header3
: They are displayed with the tags `<h2>` and `<h3>`. If you do not want to use them set the variable to `''`.

Information to connect to the LimeSurvey database. You can find this information in your LimeSurvey installation directory in the file `application/config/config.php`.

$db_host
: Hostname of the LimeSurvey database, e.g. `localhost`.

$db_database
: Name of the LimeSurvey database, e.g. `limesurvey`.

$db_user
: Name of the LimeSurvey database user, e.g. `limesurvey`.

$db_password
: Password of the user \$db_user for the database \$db_database on host \$db_host.

Base URL and user ID. The base URL is the common part of the survey URLs. The numeric user ID can be found in the column `User ID` in the table of survey administrators (in LimeSurvey: Configuration -> Manage survey administrators).

$ls_url
: Base URL of the LimeSurvey surveys, e.g. `https://www.example.com/limesurvey/index.php`.

$ls_owner
: The numeric user ID of the LimeSurvey user, that owns the surveys of the study.

Where and how to send error messages. If you do not want to get mail on errors set `$mailto = ''`.

$mailto
: Where to send error messages, e.g. `administrator@example.com`.

$subject
: Subject of the error messages.

$header
: Mail header of the error messages.

Currently there are two languages implemented, `en` (English) and `de` (German), with the corresponding language files `lang_en.php` and `lang_de.php`. To add a new language (e.g. `fr` for French), copy `lang_en.php`  to `lang_fr.php` and translate the messages. Now you can use `fr` as additional language.

$lang
: Language of the study page (i.e. `en`).


### Virtual host

If you don't want to create a virtual host for your study page, the URL of your study page is something like `https://www.example.com/mystudy`.

If you want a simpler URL, e.g. `https://mystudy.example.com` then you should create a virtual host.

For example, to create a virtual host on an Apache web server with the example names used above, create the file `/etc/apache2/sites-available/mystudy.example.com.conf` with the content:
```
<VirtualHost *:80>
        DocumentRoot /var/www/html/mystudy
        ServerName mystudy.example.com
</VirtualHost>

```

Activate the virtual host and get a SSL certificate for it.

```
a2ensite mystudy.example.com.conf
certbot -d mystudy.example.com
```


## Usage

You need to make sure that all surveys return to the study page upon completion. To achieve this, set the end URL (in Settings -> Survey Settings -> Text Elements) of each survey to the URL of the study page (in our example the end URL would be `https://mystudy.example.com`) .

To activate a questionnaire for a participant, add a new participant to the survey and set the token to the participant's personal code.

If you have access to REDCap, you can manage the surveys more conveniently with the REDCap External Module [LimeCap](https://gitlab.com/pparzer/limecap).